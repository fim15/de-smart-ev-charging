from abc import ABC, abstractmethod


class SimulationComponent(ABC):
    def __init__(self):
        super(SimulationComponent, self).__init__()

    @abstractmethod
    def step(self, simplestate):
        pass

    @abstractmethod
    def reset(self):
        pass
