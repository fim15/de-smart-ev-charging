from .simulation_component import SimulationComponent


class PeakPriceSimulationComponent(SimulationComponent):
    def __init__(self):
        # wikipedia: 5.50€ for 1kwh in 15min block per month
        self.peak_price_in_week_per_hour_per_kwh = (
            550 / 4
        )  # 4 weeks in month; price in cent
        pass

    def step(self, simplestate):
        if simplestate.is_end_of_week():
            self.peak_cost = self.peak_price_in_week_per_hour_per_kwh * self.peak
            self.peak = 0
        else:
            self.peak_cost = 0

    def reset(self):
        self.peak = 0
        self.peak_cost = 0

    def record_peak(self, peak_in_kw):
        self.peak = max(self.peak, peak_in_kw)

    def get_price_if_applicable(self):
        return self.peak_cost
