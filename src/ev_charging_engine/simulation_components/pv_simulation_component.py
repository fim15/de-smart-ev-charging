from scipy.stats import norm

from ..simplestate import SimpleState
from .simulation_component import SimulationComponent


class PVSimulationComponent(SimulationComponent):
    # TODO make these variables init params
    mean = 12
    stddev = 3
    charging_station_capacity = 50  # battery capacity

    def __init__(self, num_charging_stations):
        self.num_charging_stations = num_charging_stations
        self.amounts = [
            self.get_amount_for_hour(h) * SimpleState.hour_frac() for h in range(24)
        ]

    def get_amount_for_hour(self, hour):
        return (
            self.num_charging_stations
            * 0.44
            * self.charging_station_capacity
            * (1 / norm.pdf(self.mean, loc=self.mean, scale=self.stddev))
            * norm.pdf(hour, loc=self.mean, scale=self.stddev)
        )

    def step(self, simplestate):
        self.available_amount = self.amounts[simplestate.hour]

    def reset(self):
        self.available_amount = 0

    def get_up_to_amount(self, amount):
        # TODO scale to ints?
        taken_amount = min(self.available_amount, amount)
        self.available_amount -= taken_amount
        return taken_amount


if __name__ == "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    from src.ev_charging_engine.simplestate import SimpleState

    fig, ax = plt.subplots(1, 1)
    x = np.linspace(0, 24, 100)
    ax.plot(
        x,
        PVSimulationComponent().step(SimpleState(0, x)),
        "r-",
        lw=5,
        alpha=0.6,
        label="norm pdf",
    )
    plt.show()
