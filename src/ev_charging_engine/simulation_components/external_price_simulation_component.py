import csv

from scipy.stats import gamma

from ..simplestate import SimpleState
from .simulation_component import SimulationComponent


class ExternalPriceSimulationComponent(SimulationComponent):
    def __init__(self):
        super().__init__()
        # self.df = pd.read_csv(
        #    "data/external_energy_price_distribution.csv", delimiter=";"
        # )
        self.price_distributions = {}
        with open("data/external_energy_price_distribution.csv") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                self.price_distributions[int(row["hour"])] = (
                    float(row["alpha"]),
                    float(row["loc"]),
                    float(row["beta"]),
                )

    def step(self, simplestate):
        self.step_amount_in_kwh = 0
        self.currentstate = simplestate
        alpha, loc, beta = self.price_distributions[self.currentstate.hour]
        self.price_per_kwh = gamma.rvs(a=alpha, loc=loc, scale=beta)

    def reset(self):
        self.step_amount_in_kwh = 0
        self.currentstate = None

    def get_step_amount(self):
        return self.step_amount_in_kwh

    def get_step_power(self):
        return self.step_amount_in_kwh / SimpleState.hour_frac()

    def get_amount(self, amount_in_kwh):
        self.step_amount_in_kwh += amount_in_kwh
        return self.price_per_kwh * amount_in_kwh
