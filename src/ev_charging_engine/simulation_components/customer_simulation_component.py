# import pandas as pd
import csv

from scipy.stats import gamma, norm, poisson

from ..electric_vehicle import ElectricVehicle
from ..simplestate import SimpleState
from .simulation_component import SimulationComponent


class CustomerSimulationComponent(SimulationComponent):
    def __init__(self, frac_evs_in_traffic):
        super().__init__()
        # self.df = pd.read_csv("data/traffic_distribution.csv", delimiter=";")
        self.frac_evs_in_traffic = frac_evs_in_traffic
        self.traffic_distributions = {}
        with open("data/traffic_distribution.csv") as csvfile:
            reader = csv.DictReader(csvfile, delimiter=";")
            for row in reader:
                self.traffic_distributions[
                    (int(row["Wotag"]) - 1, int(row["Stunde"]) - 1)
                ] = (
                    float(row["alpha"]),
                    float(row["loc"]),
                    float(row["beta"]),
                )

    def reset(self):
        self.currentstate = None

    def step(self, simplestate):
        self.currentstate = simplestate

    def get_arrivals(self):
        alpha, loc, beta = self.traffic_distributions[
            (self.currentstate.day.value, self.currentstate.hour)
        ]
        number_evs = gamma.rvs(a=alpha, loc=loc, scale=beta)
        number_evs *= SimpleState.hour_frac()
        number_evs = max(0, int(number_evs * self.frac_evs_in_traffic))
        return [
            ElectricVehicle(min(max(int(100 * normv), 0), 100), poissonv + 1,)
            for normv, poissonv in zip(
                norm.rvs(loc=0.5, scale=0.15, size=number_evs),
                poisson.rvs(mu=1.5, size=number_evs),
            )
        ]
