import random

import numpy as np

from . import guarantee_alg as galg
from .simplestate import Day, SimpleState
from .simulation_components import (
    CustomerSimulationComponent,
    ExternalPriceSimulationComponent,
    PeakPriceSimulationComponent,
    PVSimulationComponent,
    SimulationComponent,
)

# from time import perf_counter as timeit

DEBUG = False


class Simulators:
    def __init__(self, num_chargers, frac_evs_in_traffic):
        self.pv_simulator = PVSimulationComponent(num_chargers)
        self.customer_simulator = CustomerSimulationComponent(frac_evs_in_traffic)
        self.eprice_simulator = ExternalPriceSimulationComponent()
        self.peak_price_simulator = PeakPriceSimulationComponent()
        self.charging_stations = ChargingStations(num_chargers)

    def reset(self):
        for simulator in self:
            simulator.reset()

    def step(self, state):
        for simulator in self:
            simulator.step(state)

    def __iter__(self):
        for attr, value in self.__dict__.items():
            yield value


class CarDoesNotExistException(Exception):
    pass


class ChargingStations(SimulationComponent):
    def __init__(self, count):
        self.count = count

    def reset(self):
        self.active = [None] * self.count

    def step(self, simplestate):
        for car in self.active:
            if car is not None:
                car.step(simplestate)

    def arrive(self, arrivals):
        none_inds = [i for i, v in enumerate(self.active) if v is None]
        chosen_inds = random.choices(none_inds, k=len(arrivals))
        for i, a in zip(chosen_inds, arrivals):
            self.active[i] = a

    def charge(self, ind, amount):
        car = self.active[ind]
        if car is None:
            raise CarDoesNotExistException()
        full, amount_used = car.charge(amount)
        if full:
            popped = self.active[ind]
            self.active[ind] = None
            return popped, amount_used
        return None, amount_used


class EVC_Station(object):
    def __init__(
        self, chargingrule, max_charge=100, num_chargers=144, frac_evs_in_traffic=0.01,
    ):
        self.frac_evs_in_traffic = frac_evs_in_traffic
        self.max_charge = max_charge
        self.chargingrule = chargingrule
        self.num_chargers = num_chargers
        self.simulators = Simulators(self.num_chargers, self.frac_evs_in_traffic)

    def seed(self, seed=None):
        if seed is not None:
            random.seed(seed)
            np.random.seed(seed)

    def reset(self):
        self.simulators.reset()
        self.state = SimpleState.default_state()
        self.step_dones = []
        self.last_loadfactor = 0

        self.tmp_galg_revenue = 0
        self.tmp_galg_cost = 0
        self.tmp_galg_electricity_used = 0
        self.tmp_galg_leftover_amount = 0
        return self.timestep()

    def timestep(self):
        # TODO revenue upfront or when done charging? or during charging?

        # _timings = {}

        # _start = timeit()
        step_revenue = self.tmp_galg_revenue
        step_cost = self.tmp_galg_cost
        step_electricity_used = self.tmp_galg_electricity_used

        invalid_action_count = 0
        for i, power in self.chargingrule.get_charging_configuration():
            assert power <= self.max_charge
            amount = power * SimpleState.hour_frac()
            try:
                done, amount_used = self.simulators.charging_stations.charge(i, amount)
                pv_amount = self.simulators.pv_simulator.get_up_to_amount(amount_used)
                leftover_amount = amount_used - pv_amount
                if leftover_amount > 0:
                    step_cost += self.simulators.eprice_simulator.get_amount(
                        leftover_amount
                    )
                if done is not None:
                    self.step_dones.append(done)
                step_electricity_used += amount_used
            except CarDoesNotExistException:
                invalid_action_count += 1

        self.simulators.peak_price_simulator.record_peak(
            self.simulators.eprice_simulator.get_step_power()
        )
        # _end = timeit()
        # _timings["apply_charging_rule"] = _end - _start

        # _start = timeit()
        peak_cost = self.simulators.peak_price_simulator.get_price_if_applicable()
        step_cost += peak_cost
        step_info = {
            "revenue": step_revenue,
            "cost": step_cost,
            "peak_cost": peak_cost,
            "electricity_used": step_electricity_used,
            "donecount": len(self.step_dones),
            "loadfactor": self.last_loadfactor,
            "unused_pv": self.simulators.pv_simulator.available_amount,
            "guarantee_cost": self.tmp_galg_cost,
            "guarantee_electricity_used": self.tmp_galg_electricity_used,
            "guarantee_kw_bought": self.tmp_galg_leftover_amount
            / SimpleState.hour_frac(),
            "invalid_action_count": invalid_action_count,
        }
        self.simulators.step(self.state)
        self.step_dones = []
        # _end = timeit()
        # _timings["step"] = _end - _start

        # _start = timeit()
        # cars arrive
        all_arrivals = self.simulators.customer_simulator.get_arrivals()
        # _end = timeit()
        # _timings["customer_sim"] = _end - _start
        # _start = timeit()

        # TODO match cars vs price

        # add cars to active contracts
        potential_arrivals, rejects = galg.filter_arrivals(
            self.simulators.charging_stations, all_arrivals, self.max_charge
        )

        self.simulators.charging_stations.arrive(potential_arrivals)
        self.last_loadfactor = (
            sum(
                1 if c is not None else 0
                for c in self.simulators.charging_stations.active
            )
            / self.num_chargers
        )
        # _end = timeit()
        # _timings["arrivals"] = _end - _start
        # _start = timeit()

        dones, amount = galg.force_charge(
            self.simulators.charging_stations, self.max_charge
        )
        self.step_dones += dones

        self.tmp_galg_revenue = 0
        self.tmp_galg_cost = 0
        self.tmp_galg_electricity_used = amount

        pv_amount = self.simulators.pv_simulator.get_up_to_amount(amount)
        self.tmp_galg_leftover_amount = amount - pv_amount
        if self.tmp_galg_leftover_amount > 0:
            self.tmp_galg_cost += self.simulators.eprice_simulator.get_amount(
                self.tmp_galg_leftover_amount
            )

        galg_info = {
            "revenue": self.tmp_galg_revenue,
            "cost": self.tmp_galg_cost,
            "electricity_used": self.tmp_galg_electricity_used,
            "kwh_bought": self.tmp_galg_leftover_amount,
            "dones": dones,
        }
        # _end = timeit()
        # _timings["force_charging"] = _end - _start

        # _start = timeit()
        # charge by charging rule
        self.chargingrule.setup(
            self.simulators.charging_stations,
            self.simulators,
            galg_info,
            step_info,
            self.state,
        )

        # TODO get new prices
        # TODO set new prices

        if DEBUG:
            print(
                "active: {}".format(
                    "; ".join(
                        [
                            "({},{})".format(ev.charge_desired, ev.time_to_leave)
                            for ev in self.simulators.charging_stations.active
                            if ev is not None
                        ]
                    )
                )
            )

        self.state.advance()

        # _end = timeit()
        # _timings["other"] = _end - _start
        # step_info['timings'] = _timings
        self.last_step_info = step_info
        return step_info
