from . import AbstractChargingRule


class ConstantChargingRule(AbstractChargingRule):
    def __init__(self, const_charging_rate):
        self.const_charging_rate = const_charging_rate
        self.to_charge = []

    def setup(self, stations, *args, **kwargs):
        self.to_charge = [i for i, v in enumerate(stations.active) if v is not None]

    def get_charging_configuration(self):
        return zip(self.to_charge, [self.const_charging_rate] * len(self.to_charge))
