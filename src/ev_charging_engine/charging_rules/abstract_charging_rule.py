from abc import ABC, abstractmethod


class AbstractChargingRule(ABC):
    def __init__(self):
        super(AbstractChargingRule, self).__init__()

    @abstractmethod
    def setup(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_charging_configuration(self):
        pass
