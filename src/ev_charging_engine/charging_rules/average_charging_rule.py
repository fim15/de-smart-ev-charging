from . import AbstractChargingRule


class AverageChargingRule(AbstractChargingRule):
    def __init__(self, max_charging_rate):
        self.max_charging_rate = max_charging_rate
        self.rule = []

    def setup(self, stations, *args, **kwargs):
        self.rule = [
            (i, min(self.max_charging_rate, v.charge_desired / v.time_to_leave))
            for i, v in enumerate(stations.active)
            if v is not None
        ]

    def get_charging_configuration(self):
        return self.rule
