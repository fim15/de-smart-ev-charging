from math import exp

from .simplestate import SimpleState


class ElectricVehicle:
    def __init__(
        self,
        charge_desired,
        time_to_leave,
        maximum_charging_speed=None,
        willingness_to_pay=None,
    ):
        self.charge_desired = charge_desired
        self.initial_time_to_leave = time_to_leave
        self.maximum_charging_speed = maximum_charging_speed
        self.time_to_leave = time_to_leave
        self.willingness_to_pay = willingness_to_pay

    def step(self, simplestate):
        self.time_to_leave -= SimpleState.hour_frac()
        if self.time_to_leave <= 0 and self.charge_desired > 0:
            # raise ValueError(
            print(
                f"ERROR: Guratantee failed - TTL = 0, leftover DC: {self.charge_desired}"
            )
        # )

    def get_willingness_to_pay(self):
        if self.willingness_to_pay is not None:
            return self.willingness_to_pay
        else:
            return 60 * exp(0.06 * self.initial_time_to_leave)

    def charge(self, amount):
        self.charge_desired -= amount
        if self.charge_desired < 0:
            # raise ValueError("Overcharged!!!")
            # print("WARNING: Overcharged")
            pass
        if self.charge_desired <= 0:
            overflow = self.charge_desired
            self.charge_desired = 0
            return True, amount + overflow
        return False, amount
