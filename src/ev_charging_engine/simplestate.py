from enum import Enum, unique

from dataclasses import dataclass


@unique
class Day(Enum):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

    def next(self):
        return Day((self.value + 1) % len(Day))


@dataclass
class SimpleState:
    MIN_STEP = 15
    day: Day
    hour: int
    min: int

    def advance(self):
        self.min += self.MIN_STEP
        if self.min >= 60:
            self.hour += 1
            if self.hour > 23:
                self.day = self.day.next()
            self.hour %= 24
        self.min %= 60

    def is_end_of_week(self):
        return (
            self.day.value == len(Day) - 1
            and self.hour == 23
            and self.min >= (60 - self.MIN_STEP)
        )

    @staticmethod
    def steps_per_week():
        return len(Day) * 24 * (60 // SimpleState.MIN_STEP)

    @staticmethod
    def default_state():
        return SimpleState(Day.MONDAY, 0, 0)

    @staticmethod
    def hour_frac():
        return SimpleState.MIN_STEP / 60


if __name__ == "__main__":
    # simple tests
    state = SimpleState.default_state()
    # simulate one week
    for i in range(SimpleState.steps_per_week()):
        print(state)
        state.advance()
