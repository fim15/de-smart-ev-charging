from .simplestate import SimpleState


# ASSUMPTION: cars can be fully charged in initial TTL
def filter_arrivals(stations, arrivals, max_charge):
    free_places = sum(1 if c is None else 0 for c in stations.active)
    if len(arrivals) <= free_places:
        return arrivals, []
    potentials = arrivals[:free_places]
    rejects = arrivals[free_places:]
    # assert that still chargable
    assert (
        sum(c.charge_desired if c is not None else 0 for c in stations.active)
        + sum(c.charge_desired for c in potentials)
    ) <= (len(stations.active) * max_charge)
    return potentials, rejects


def force_charge(stations, max_charge):
    # which cars have ttl 1 -> charge them
    # stations.chare(car, amount)
    max_amount = max_charge * SimpleState.hour_frac()
    potentials = [
        (i, c, int(c.time_to_leave * 60 / SimpleState.MIN_STEP))
        for i, c in enumerate(stations.active)
        if c is not None
    ]
    min_step_charge = [
        (i, (c.charge_desired - (steps - 1) * max_amount)) for i, c, steps in potentials
    ]
    todo = [(i, step_charge) for i, step_charge in min_step_charge if step_charge > 0]
    dones = []
    amount = 0
    for i, step_charge in todo:
        assert step_charge <= max_amount
        d, a = stations.charge(i, step_charge)
        amount += a
        if d is not None:
            dones.append(d)

    return dones, amount
