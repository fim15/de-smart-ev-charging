from ev_charging_engine.charging_rules import AbstractChargingRule


class ObservationChargingRule(AbstractChargingRule):
    def __init__(self, amount_per_mode):
        self.amount_per_mode = amount_per_mode

    def reset(self):
        self.charging_stations = None
        self.simulators = None
        self.galg_info = None
        self.step_info = None
        self.simplestate = None
        self.charging_configuration = []

    def setup(
        self,
        charging_stations,
        simulators,
        galg_info,
        step_info,
        simplestate,
        *args,
        **kwargs,
    ):
        self.charging_stations = charging_stations
        self.simulators = simulators
        self.galg_info = galg_info
        self.step_info = step_info
        self.simplestate = simplestate

    def apply_action(self, action):
        self.charging_configuration = [
            (i, self.amount_per_mode[mode])
            for i, mode in enumerate(action)
            if mode != 0
        ]

    def get_charging_configuration(self):
        return self.charging_configuration
