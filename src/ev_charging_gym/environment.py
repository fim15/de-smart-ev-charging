import random
import sys

import gym
import numpy as np
from ev_charging_engine.evc_station import EVC_Station

from .observation import convert_to_observation, get_action_space, get_observation_space
from .observation_charging_rule import ObservationChargingRule


class EVChargingEnv(gym.Env):
    metadata = {"render.modes": []}

    def __init__(self, **kwargs):
        self.initialized = False
        self.kwargs = kwargs
        self.env_config = {"charging_rule": [0, 10, 20, 50, 100]}
        self.action_space = get_action_space(len(self.env_config["charging_rule"]))
        self.observation_space = get_observation_space()

    def step(self, action):
        """

        Parameters
        ----------
        action :

        Returns
        -------
        ob, reward, episode_over, info : tuple
            ob (object) :
                an environment-specific object representing your observation of
                the environment.
            reward (float) :
                amount of reward achieved by the previous action. The scale
                varies between environments, but the goal is always to increase
                your total reward.
            episode_over (bool) :
                whether it's time to reset the environment again. Most (but not
                all) tasks are divided up into well-defined episodes, and done
                being True indicates the episode has terminated. (For example,
                perhaps the pole tipped too far, or you lost your last life.)
            info (dict) :
                 diagnostic information useful for debugging. It can sometimes
                 be useful for learning (for example, it might contain the raw
                 probabilities behind the environment's last state change).
                 However, official evaluations of your agent are not allowed to
                 use this for learning.
        """
        self.charging_rule.apply_action(action)
        reward = self.advance()
        return convert_to_observation(self.charging_rule), reward, False, {}  # TODO

    def advance(self):
        step_info = self.station.timestep()
        return -step_info["cost"]

    def seed(self, seed=None):
        if seed is not None:
            random.seed(seed)
            np.random.seed(seed)

    def reset(self):
        if not self.initialized:
            self.initialized = True
            self.charging_rule = ObservationChargingRule(
                self.env_config["charging_rule"]
            )
            self.station = EVC_Station(self.charging_rule, **self.kwargs)
        self.charging_rule.reset()
        self.station.reset()

        return convert_to_observation(
            self.charging_rule
        )  # TODO, optionally add info dict for invalid actions

    def render(self, mode="human"):
        outfile = StringIO() if mode == "ansi" else sys.stdout

        # outfile.write("Debug Yay\n")

        if mode != "human":
            with closing(outfile):
                return outfile.getvalue()

    def close(self):
        # remove the engine
        pass
