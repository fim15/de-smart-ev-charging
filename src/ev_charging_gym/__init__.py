from gym.envs.registration import register

from .environment import EVChargingEnv

register(id="EVCharging-v0", entry_point="ev_charging_gym:EVChargingEnv")
