import numpy as np
from ev_charging_engine.simplestate import SimpleState
from gym.spaces import Box, Dict, Discrete, MultiBinary, MultiDiscrete, Tuple, flatdim

NUM_CHARGING_SPACES = 144


# TODO if no external normalization => normalize to -1,1
def get_action_space(num_charge_options):
    return MultiDiscrete([num_charge_options] * NUM_CHARGING_SPACES)


def observation_space_dict():
    space = Dict(
        {
            # TODO test discrete for ttl
            # TODO interleave desired charge and ttl?
            # TODO multidimensional box for CNN
            "current_charging": Dict(
                {
                    # low = np.tile([0, 0], NUM_CHARGING_SPACES)
                    # high = np.tile([1, 24], NUM_CHARGING_SPACES)
                    "desired_charge": Box(
                        low=0, high=100, shape=(NUM_CHARGING_SPACES,)
                    ),
                    "ttl": Box(low=0, high=24, shape=(NUM_CHARGING_SPACES,)),
                }
            ),
            "pv_amount": Box(0, np.inf, shape=(1,)),
            "external_price": Box(-np.inf, np.inf, shape=(1,)),
            "simplestate": Tuple((Discrete(7), Discrete(24), Discrete(4))),
            "last_peak": Box(0, np.inf, shape=(1,)),
        }
    )
    # print("flat dimensions: ", flatdim(space))
    # assert flatdim(space) == 625
    return space


def get_observation_space():
    # transform everything to a biiiig 1D box vector
    space = observation_space_dict()
    flattened_list = flattenspace(space)
    flattened_box = spaces_to_box(flattened_list)
    return flattened_box


def spaces_to_box(space_list):
    low = []
    high = []
    for space in space_list:
        if isinstance(space, Box):
            low.append(space.low)
            high.append(space.high)
        elif isinstance(space, Discrete):
            low.append(np.zeros(shape=(space.n,)))
            high.append(np.ones(shape=(space.n,)))
        else:
            raise ValueError(f"Not implemented for {type(space)}")
    low = np.hstack(low)
    high = np.hstack(high)
    return Box(low, high)


def flattenspace(space):
    if isinstance(space, Tuple):
        return sum((flattenspace(s) for s in space.spaces), [])
    elif isinstance(space, Dict):
        return sum((flattenspace(s) for s in space.spaces.values()), [])
    else:
        return [space]


def observation_to_box(observation):
    spec = observation_space_dict()
    box = rec_obs_to_box(spec, observation)
    return box


def rec_obs_to_box(space, observation):
    if isinstance(space, Dict):
        return np.hstack(
            [
                rec_obs_to_box(space, observation[key])
                for key, space in space.spaces.items()
            ]
        )
    elif isinstance(space, Tuple):
        return np.hstack(
            [
                rec_obs_to_box(space, observation[key])
                for key, space in enumerate(space.spaces)
            ]
        )
    elif isinstance(space, Box):
        return np.array(observation)
    elif isinstance(space, Discrete):
        x = np.zeros(space.n)
        x[observation] = 1
        return x
    else:
        raise ValueError(f"Unimplemented for space {space}")


def convert_to_observation(charging_rule):
    desired_charge = np.zeros(shape=(NUM_CHARGING_SPACES,))
    ttl = np.zeros(shape=(NUM_CHARGING_SPACES,))
    for i, car in enumerate(charging_rule.charging_stations.active):
        if car is not None:
            desired_charge[i] = car.charge_desired
            ttl[i] = car.time_to_leave

    observation = {
        "current_charging": {"desired_charge": desired_charge, "ttl": ttl,},
        "pv_amount": charging_rule.simulators.pv_simulator.available_amount,
        "external_price": charging_rule.simulators.eprice_simulator.price_per_kwh,
        # TODO adjust for 15 min
        "simplestate": (
            charging_rule.simplestate.day.value,
            charging_rule.simplestate.hour,
            charging_rule.simplestate.min % SimpleState.MIN_STEP,
        ),
        "last_peak": charging_rule.simulators.peak_price_simulator.peak,
    }
    box = observation_to_box(observation)
    # flattening will happen by gym
    return box
