import argparse
import statistics
import time
from os import listdir
from os.path import basename, exists, isdir, isfile, join

import gym
import metrics
from env_util import hyperparams_to_envfn
from ev_charging_engine.simplestate import SimpleState
from hyperparams import load_hyperparams, to_uid
from stable_baselines import PPO2
from stable_baselines.common.vec_env import DummyVecEnv, VecNormalize
from tensorboard_logging import Logger as TensorboardLogger
from tqdm import tqdm


def run_test(run, checkpointfile, test_env, test_seed, test_writer):
    WEEKS = 8  # TODO maybe 8 parallel one week
    N = SimpleState.steps_per_week()

    test_env.seed(test_seed)
    obs = test_env.reset()

    model = PPO2.load(checkpointfile)

    results = {}
    for i in tqdm(range(N * WEEKS)):
        action, _state = model.predict(obs, deterministic=True)
        obs, reward, done, info = test_env.step(action)
        info = test_env.get_attr("station")[0].last_step_info
        for k in info:
            if k not in results:
                results[k] = []
            results[k].append(info[k])

    results = metrics.to_metrics(results)

    for metric, data in results.items():
        mean = statistics.mean(data)
        std = statistics.stdev(data)
        test_writer.log_scalar("{}/mean".format(metric), mean, run)
        test_writer.log_scalar("{}/std".format(metric), std, run)
        print(f"[{run}] {metric}: mean = {mean}, stdev = {std}")

        test_writer.writer.flush()


if __name__ == "__main__":

    checkpoint_dir = ""
    tensorboard_dir = ""

    parser = argparse.ArgumentParser(description="Testing model checkpoints.")

    parser.add_argument(
        "-i",
        "--interval",
        type=float,
        default=10.000,
        help="the time interval before the next check for new checkpoints (in seconds, float possible)",
    )
    parser.add_argument(
        "-l",
        "--last-checkpoint",
        type=int,
        help="if this checkpoint is encountered, then terminate",
    )
    parser.add_argument(
        "-t",
        "--terminate",
        action="store_true",
        help="process all existing checkpoints and immediately terminate, overrides interval",
    )
    # TODO utilize
    parser.add_argument(
        "-j", "--cpus", type=int, default=1, help="the number of cpu cores to use"
    )

    parser.add_argument(
        "-p", "--prefix", help="text before the checkpoint number in the file name"
    )
    parser.add_argument(
        "-s", "--suffix", help="text after the checkpoint number in the file name"
    )

    parser.add_argument(
        "-u", "--hyperparams", help="a hyperparameter definition file",
    )
    # TODO format as directory (trailing / and all)
    parser.add_argument(
        "-c", "--checkpoint-dir", help="the directory containing the checkpoint files",
    )
    parser.add_argument(
        "-b", "--tensorboard-dir", help="the directory to store tensorboard logs to",
    )

    parser.add_argument(
        "--seed", type=int, default=7357, help="the seed used for testing"
    )
    parser.add_argument(
        "-w", "--weeks", type=int, default=1000, help="number of test games to play"
    )

    args = parser.parse_args()

    hyperparams = load_hyperparams(args.hyperparams)
    uid = to_uid(hyperparams)
    print("UID: ", uid)

    if args.checkpoint_dir is None:
        args.checkpoint_dir = f"{settings.MODEL_PATH}/checkpoints/model_{uid}"
    if args.tensorboard_dir is None:
        args.tensorboard_dir = "./models/test/test_{}".format(uid)

    tested_file = ".tested"

    checkpointfiles = {}
    if exists(join(args.tensorboard_dir, tested_file)):
        print("found .tested file")
        with open(join(args.tensorboard_dir, tested_file), "r") as tested:
            lines = [line.rstrip("\n") for line in tested]
            lines = [
                line[len(args.checkpoint_dir) :]
                for line in lines
                if line.startswith(args.checkpoint_dir)
            ]
            print("found {} already completed testruns".format(len(lines)))
            checkpointfiles = {
                "done-{}".format(i): basename(line) for i, line in enumerate(lines)
            }

    env_fn = hyperparams_to_envfn(hyperparams)
    test_env = DummyVecEnv([env_fn])
    test_env = VecNormalize(test_env, training=True, norm_obs=True, norm_reward=True)

    test_writer = TensorboardLogger(args.tensorboard_dir)

    terminate = False
    while not terminate:
        terminate = args.terminate

        # find new checkpoints
        new_checkpointfiles = []
        if isdir(args.checkpoint_dir):
            new_checkpointfiles = [
                f
                for f in listdir(args.checkpoint_dir)
                if isfile(join(args.checkpoint_dir, f))
                if f.startswith(args.prefix)
                if f.endswith(args.suffix)
                if f not in checkpointfiles.values()
            ]
        else:
            print("no checkpoint dir yet")
            print(f"dir: '{args.checkpoint_dir}'")

        if new_checkpointfiles:
            new_checkpointfiles = {
                int(
                    filename[len(args.prefix) : len(filename) - len(args.suffix)]
                ): filename
                for filename in new_checkpointfiles
            }

            # test all checkpoint files
            for i, filename in sorted(new_checkpointfiles.items()):
                print("testing checkpoint #{}".format(i))
                run_test(
                    i,
                    join(args.checkpoint_dir, filename),
                    test_env,
                    args.seed,
                    test_writer,
                )

                with open(join(args.tensorboard_dir, tested_file), "a") as tested:
                    tested.write("{}\n".format(join(args.checkpoint_dir, filename)))

                if i == args.last_checkpoint:
                    print("Encountered last checkpoint {} - terminating".format(i))
                    exit()

            checkpointfiles.update(new_checkpointfiles)
        else:
            print("no new checkpoints")

        if args.terminate:
            print("Processed all checkpoint files - terminating")
            exit()

        print("waiting for new checkpoints...")
        # sleep interval
        time.sleep(args.interval)
