import json
import os
import subprocess


def load_hyperparams(hyperparams_file="hyperparams.json"):
    # default_net_arch = [dict(pi=[64, 64], vf=[64, 64])]
    # shared_net_arch = [64, 64, 64]
    # seperate_net_arch = [dict(pi=[128, 128, 128], vf=[128, 128, 128])]
    # mixed_net_arch = [
    #    128,
    #    dict(pi=[128, 128], vf=[128, 128]),
    # ]  # can also exclude vf or pi

    if not os.path.isfile(hyperparams_file):
        print("ERROR no '{}' found - ABORTING".format(hyperparams_file))
        exit(1)

    hyperparams = None
    with open(hyperparams_file) as f:
        hyperparams = json.load(f)
    if hyperparams is None:
        print("ERROR while loading hyperparams - ABORTING")
        exit(1)

    return hyperparams


def format_netarch(netarch):
    if type(netarch[-1]) is not dict:
        return "{}_0_0".format("-".join(netarch))
    shared = netarch[:-1]
    seperate = netarch[-1]
    return "{}_{}_{}".format(
        "0" if len(shared) == 0 else "-".join((str(s) for s in shared)),
        "-".join((str(s) for s in seperate["pi"])) if "pi" in seperate else "0",
        "-".join((str(s) for s in seperate["vf"])) if "vf" in seperate else "0",
    )


def to_uid(hyperparams):
    command = "git describe --tags --abbrev=0"
    try:
        out = subprocess.check_output(command.split(), stderr=subprocess.STDOUT)
        gittag = out.decode("utf8").rstrip("\n")
        # do something with output
    except subprocess.CalledProcessError as e:
        # There was an error - command exited with non-zero code
        print("Error collecting git tag", e)
        gittag = None

    uid = "{}_{}_ep{}_envs{}_nst{}_{}_b{}{}".format(
        gittag,
        hyperparams["model"],
        hyperparams["numepisodes"],
        hyperparams["nenvs"],
        hyperparams["n_steps"],
        format_netarch(hyperparams["net_arch"]),
        hyperparams["beta"],
        f"_{hyperparams['uid_suffix']}" if "uid_suffix" in hyperparams else "",
    )
    return uid
