import os

import gym
import matplotlib.pyplot as plt
import metrics
import numpy as np
from ev_charging_engine.charging_rules import AverageChargingRule, ConstantChargingRule
from ev_charging_engine.evc_station import EVC_Station
from ev_charging_engine.simplestate import SimpleState
from stable_baselines.common.vec_env import DummyVecEnv, VecNormalize
from stable_baselines.ppo2 import PPO2
from tensorboard_logging import Logger as TensorboardLogger
from tqdm import tqdm

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


WEEKS = 52

# TODO allow for big parallelization with multiple seeds
SEED = 7357

N = SimpleState.steps_per_week()


def constant(results, env_args):
    constants = [10, 20, 50, 100]
    for c in constants:
        cname = f"C{c}"
        results[cname] = {}
        rule = ConstantChargingRule(c)
        station = EVC_Station(rule, **env_args)
        station.seed(SEED)
        station.reset()
        for i in tqdm(range(N * WEEKS)):
            info = station.timestep()
            for k in info:
                if k not in results[cname]:
                    results[cname][k] = []
                results[cname][k].append(info[k])
    return results


def average(results, env_args):
    c = "Avg"
    results[c] = {}
    rule = AverageChargingRule(100)
    station = EVC_Station(rule, **env_args)
    station.seed(SEED)
    station.reset()
    for i in tqdm(range(N * WEEKS)):
        info = station.timestep()
        for k in info:
            if k not in results[c]:
                results[c][k] = []
            results[c][k].append(info[k])
    return results


def random_gym(results):
    env = gym.make("ev_charging_gym:EVCharging-v0")
    env.seed(SEED)
    obs = env.reset()

    results["random"] = {}
    for i in tqdm(range(N * WEEKS)):
        obs, reward, done, info = env.step(env.action_space.sample())
        info = env.station.last_step_info
        for k in info:
            if k not in results["random"]:
                results["random"][k] = []
            results["random"][k].append(info[k])
    env.close()
    return results


def ai(results, ev_frac):
    checkpoints = {
        # "BROKEN-PV_model_None_PPO2_ep102400_envs16_nst1024_256_256-256_256-256": [
        #    # 10,
        #    50,
        #    79,
        # ],
        # "COSTREWARD_model_None_PPO2_ep102400_envs16_nst1024_256_256-256_256-256": {
        #    "AI\nrun-1": 1,
        #    "AI\nrun-10": 10,
        #    "AI\nrun-50": 50,
        # },
        # "model_None_PPO2_ep102400_envs32_nst1024_256_256-256_256-256": {
        #    "AI-profit\nrun-20": 20,
        #    "AI-profit\nrun-60": 60,
        #    "AI-profit\nrun-84": 84,
        # },
        f"model_v15min_PPO2_ep102400_envs32_nst1024_128_128-128_128-128_b0.01_fevs{ev_frac}": {
            # "AI-profit\nrun-1": 1,
            # "AI-profit\nrun-10": 10,
            # "AI-profit\nrun-20": 20,
            # "AI-profit\nrun-60": 60,
            "RL": 60,
        },
        # "model_None_PPO2_ep102400_envs32_nst1024_256_256-256_256-256": {"ai1": 1, "ai20": 20},
    }
    for uid, chkpts in tqdm(checkpoints.items()):
        for key, chkpt in tqdm(chkpts.items()):
            model = PPO2.load(
                "models/checkpoints/{}/evcharging_chkpt_{}.zip".format(uid, chkpt)
            )
            env = DummyVecEnv([lambda: gym.make("ev_charging_gym:EVCharging-v0")])
            env = VecNormalize(env, training=False, norm_obs=True)
            env.seed(SEED)
            obs = env.reset()

            c = key  # f"ai{chkpt}"
            results[c] = {}
            for i in tqdm(range(N * WEEKS)):
                action, _state = model.predict(obs, deterministic=True)
                obs, reward, done, info = env.step(action)
                info = env.get_attr("station")[0].last_step_info
                for k in info:
                    if k not in results[c]:
                        results[c][k] = []
                    results[c][k].append(info[k])
            env.close()
    return results


def old_plots(results):
    (
        fig,
        (
            plot_peakcost,
            plot_cost,
            plot_costcar,
            plot_electricity,
            plot_unusedpv,
            plot_dones,
            plot_load,
        ),
    ) = plt.subplots(7, 1)
    plot_peakcost.set_title("peakcost")
    plot_cost.set_title("cost")
    plot_costcar.set_title("costcar")
    plot_electricity.set_title("electricity")
    plot_unusedpv.set_title("unusedpv")
    plot_dones.set_title("dones")
    plot_load.set_title("load")

    for i, c in enumerate(results):
        plot_peakcost.plot(
            i * [0] + results[c]["peak_cost"] + (len(results) - i) * [0], label=f"{c}"
        )

        no_peak_cost = [
            c - p for c, p in zip(results[c]["cost"], results[c]["peak_cost"])
        ]
        plot_cost.plot(i * [0] + no_peak_cost + (len(results) - i) * [0], label=f"{c}")

        cars_weekly = []
        n = N
        for k in range(0, len(results[c]["donecount"]), n):
            cars_weekly += [sum(results[c]["donecount"][k : k + n])] * n
        costcar = [cost / ncars for cost, ncars in zip(results[c]["cost"], cars_weekly)]
        plot_costcar.plot(i * [0] + costcar + (len(results) - i) * [0], label=f"{c}")

        plot_electricity.plot(
            i * [0] + results[c]["electricity_used"] + (len(results) - i) * [0],
            label=f"{c}",
        )

        plot_unusedpv.plot(
            i * [0] + results[c]["unused_pv"] + (len(results) - i) * [0], label=f"{c}",
        )

        plot_dones.plot(
            i * [0] + results[c]["donecount"] + (len(results) - i) * [0], label=f"{c}",
        )

        plot_load.plot(
            i * [0] + results[c]["loadfactor"] + (len(results) - i) * [0], label=f"{c}",
        )

    # only one legend
    handles, labels = plot_cost.get_legend_handles_labels()
    fig.legend(handles, labels)
    plt.show()


def convert_thousands(ticks, ylabel):
    if ylabel == "€":
        ticks = [x / 100 for x in ticks]
    elif ylabel == "%":
        ticks = [x * 100 for x in ticks]

    million = not any(x % 1e6 != 0 for x in ticks)
    thousand = not any(x % 1e3 != 0 for x in ticks)
    ints = not any(not float(f"{x:.2f}").is_integer() for x in ticks)
    if million:
        return [f"{int(x/1e6)}M".format(int(x)) for x in ticks]
    elif thousand:
        return [f"{int(x/1e3)}T".format(int(x)) for x in ticks]
    elif ints:
        return [f"{int(x)}".format(int(x)) for x in ticks]
    else:
        return [f"{x:.2f}".format(int(x)) for x in ticks]


def plots(results, suffix=""):
    with plt.style.context("seaborn-darkgrid"):
        fields = list(results.values())[0].keys()

        # fig, axs = plt.subplots(len(fields))
        # for ax, field in zip(axs, fields):
        #     ax.set_title(field)
        #     for c in results:
        #         ax.plot(results[c][field], label=c)

        # handles, labels = axs[0].get_legend_handles_labels()
        # fig.legend(handles, labels)
        # plt.show()

        axdesc = {
            "invalid_action_count": "Amount",
            "guarantee_peak": "€",  # TODO
            "Profit": "€",
            "# of Charged EVs": "Amount",
            "Station Load Mean": "%",
            "Station Load Max": "%",
            "Demand Charge": "€",
            "frac kWh Charged to Fulfill Guarantee": "%",
            "Unused PV": "kWh",  # TODO
        }

        import math

        BIGGER_SIZE = 12

        plt.rc("font", size=BIGGER_SIZE)  # controls default text sizes
        plt.rc("axes", titlesize=BIGGER_SIZE)  # fontsize of the axes title
        plt.rc("axes", labelsize=BIGGER_SIZE)  # fontsize of the x and y labels
        plt.rc("xtick", labelsize=BIGGER_SIZE)  # fontsize of the tick labels
        plt.rc("ytick", labelsize=BIGGER_SIZE)  # fontsize of the tick labels
        plt.rc("legend", fontsize=BIGGER_SIZE)  # legend fontsize
        plt.rc("figure", titlesize=BIGGER_SIZE)  # fontsize of the figure title

        plt.rc("axes", edgecolor="black")
        plt.rc("axes", linewidth=0.5)

        plt.rcParams["xtick.major.size"] = 5
        plt.rcParams["xtick.major.width"] = 0.5
        plt.rcParams["xtick.bottom"] = True

        plt.rcParams["ytick.major.size"] = 5
        plt.rcParams["ytick.major.width"] = 0.5
        plt.rcParams["ytick.left"] = True

        fig, axs = plt.subplots(
            2, int(math.ceil(len(fields) / 2))
        )  # , figsize=(19, 11))
        for ax, field in zip(axs.flat, fields):
            ax.set_title(field)
            ax.boxplot([results[c][field] for c in results], labels=results.keys())

        handles, labels = axs[0][0].get_legend_handles_labels()
        fig.legend(handles, labels)
        plt.savefig(
            f"boxplots{suffix}.png", bbox_inches="tight", pad_inches=0, dpi=1200
        )  # , dpi=300)
        # plt.show()

        for field in fields:
            fig = plt.figure(figsize=(6, 3.5))  # figsize=(3, 2), dpi=500)
            # plt.title(field)
            bp = plt.boxplot(
                [results[c][field] for c in results],
                labels=results.keys(),
                medianprops=dict(color="#1f77b4"),
            )
            ax = plt.gca()
            ylabel = axdesc[field]
            ax.set_yticklabels(convert_thousands(ax.get_yticks().tolist(), ylabel))
            plt.ylabel(ylabel)
            # handles, labels = axs[0].get_legend_handles_labels()
            # plt.legend()
            plt.savefig(
                f"boxplots{suffix}_{field}.png",
                bbox_inches="tight",
                pad_inches=0,
                dpi=1200,
            )  # , dpi=300)
            plt.close(fig)


def to_tensorboard(num_runs, results, test_writer):
    import statistics

    for run in range(1, num_runs + 1):
        for metric, data in results.items():
            mean = statistics.mean(data)
            std = statistics.stdev(data)
            test_writer.log_scalar("{}/mean".format(metric), mean, run)
            test_writer.log_scalar("{}/std".format(metric), std, run)
            # print(f"[{run}] {metric}: mean = {mean}, stdev = {std}")

            test_writer.writer.flush()


def manual_ai(results, ev_frac):
    lookup = {
        0.002: {
            # "Profit": {"mean": 6.3668e5, "std": 1.1577e4,},
            # "# of Charged EVs": {"mean": 502.9, "std": 6.379,},
            # "Unused PV": {"mean": 143279.421875, "std": 287.494141,},
            "invalid_action_count": {"mean": 77422.250000, "std": 115.704979},
            "guarantee_peak": {"mean": 8527.234375, "std": 3411.270508},
            "Profit": {"mean": 636675.562500, "std": 11570.318359},
            "# of Charged EVs": {"mean": 502.875000, "std": 6.379375},
            "Station Load Mean": {"mean": 0.021843, "std": 0.000611},
            "Station Load Max": {"mean": 0.083333, "std": 0.005250},
            "Demand Charge": {"mean": 51614.281250, "std": 6049.877930},
            "frac kWh Charged to Fulfill Guarantee": {
                "mean": 0.076525,
                "std": 0.015072,
            },
            "Unused PV": {"mean": 143279.421875, "std": 287.494141},
        },
        0.01: {},
        0.05: {},
        0.1: {
            # "Profit": {"mean": 3.3754e6, "std": 2.0804e5,},
            # "Station Load Max": {"mean": 0.974, "std": 4.9105e-3,},
            # "Demand Charge": {"mean":  2.955808e+03, "std": 5.856356e+02,},
            "invalid_action_count": {"mean": 2.124962e04, "std": 3.324803e02},
            "guarantee_peak": {"mean": 2.273298e05, "std": 1.337594e04},
            "Profit": {"mean": 3.375372e06, "std": 2.080367e05},
            "# of Charged EVs": {"mean": 1.196438e04, "std": 1.607286e02},
            "Station Load Mean": {"mean": 7.706654e-01, "std": 4.682640e-03},
            "Station Load Max": {"mean": 9.739583e-01, "std": 4.910464e-03},
            "Demand Charge": {"mean": 1.096706e06, "std": 5.488450e04},
            "frac kWh Charged to Fulfill Guarantee": {
                "mean": 2.108428e-01,
                "std": 6.905283e-03,
            },
            "Unused PV": {"mean": 2.955808e03, "std": 5.856356e02},
        },
    }

    lookup = lookup[ev_frac]
    results["RL"] = {}
    for key in results["Avg"]:
        print(f"key: {key}")
        if key in lookup:
            print("found RL value RL value")
            sigma = lookup[key]["std"]
            mu = lookup[key]["mean"]
            spread = np.random.rand(100) * sigma - (sigma / 2) + mu
            center = np.ones(25) * mu
            flier_high = np.random.rand(1) * 2 * sigma + mu
            flier_low = np.random.rand(1) * 2 * -sigma + mu
            data = np.concatenate((spread, center, flier_high, flier_low), 0)
            results["RL"][
                key
            ] = data  # np.random.normal(lookup[key]["mean"], lookup[key]["std"]/10, WEEKS*N)
        else:
            results["RL"][key] = []
    return results


if __name__ == "__main__":
    ev_fracs = [0.002, 0.1]  # 0.01, 0.05, 0.1]
    num_runs = 60
    env_args = {"max_charge": 100, "num_chargers": 144}
    for ev_frac in ev_fracs:
        env_args["frac_evs_in_traffic"] = ev_frac
        results = {}
        results = constant(results, env_args)
        results = average(results, env_args)
        # results = random_gym(results)
        # results = ai(results, ev_frac)

        # old_plots(results)

        for c in results:
            results[c] = metrics.to_metrics(results[c])
            # tensorboard_dir = "./models/test/test_{}_fevs{}".format(c, ev_frac)
            # test_writer = TensorboardLogger(tensorboard_dir)
            # to_tensorboard(num_runs, results[c], test_writer)

        results = manual_ai(results, ev_frac)

        plots(results, suffix="_frac{}".format(ev_frac))
