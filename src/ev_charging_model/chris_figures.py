from math import exp

import numpy as np
from matplotlib import pyplot
from scipy.stats import lognorm, poisson

if __name__ == "__main__":
    fig, ax = pyplot.subplots(1, 1)
    x = [x for x in range(0, 8)]
    y = [poisson.pmf(x, mu=1.5) for x in x]
    # poisson.rvs(mu=1.5) + 1
    ax.stem([x + 1 for x in x], y, basefmt=" ")
    pyplot.xlabel("TTL")
    pyplot.ylabel("Probability")
    pyplot.ylim(bottom=0)
    pyplot.savefig("ttl.png", bbox_inches="tight", pad_inches=0, dpi=1200)

    fig, ax = pyplot.subplots(1, 1)
    x = np.linspace(0, 1, 1000)
    ax.plot(x, lognorm.pdf(x, s=0.35, scale=exp(-1)))
    pyplot.ylim(bottom=0)
    pyplot.xlabel("SOC")
    pyplot.ylabel("Probability")
    pyplot.savefig("soc.png", bbox_inches="tight", pad_inches=0, dpi=1200)
    pyplot.show()
