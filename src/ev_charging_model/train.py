import json
import os

from env_util import hyperparams_to_envfn
from hyperparams import load_hyperparams, to_uid
from stable_baselines import PPO2
from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common.vec_env import SubprocVecEnv, VecNormalize
from tensorboard_logging import Logger as TensorboardLogger

if __name__ == "__main__":
    hyperparams = load_hyperparams()
    uid = to_uid(hyperparams)
    print("UID: ", uid)

    num_runs = 500  # 100
    if "num_runs" in hyperparams:
        num_runs = int(hyperparams["num_runs"])

    tensorboard_log_dir = "./models/train/tensorboard_{}".format(uid)
    checkpoint_path = "./models/checkpoints/model_{}".format(uid)
    os.makedirs(checkpoint_path, exist_ok=True)
    checkpointfilename_prefix = "evcharging_chkpt_"
    checkpointfilename = checkpointfilename_prefix + "{}"

    metadatafile = os.path.join(checkpoint_path, "metadata.json")
    if os.path.isfile(metadatafile):
        with open(metadatafile) as f:
            loaded_hyperparams = json.load(f)
            if json.dumps(loaded_hyperparams, sort_keys=True) != json.dumps(
                json.loads(json.dumps(hyperparams)), sort_keys=True
            ):
                print("ERROR: Hyperparams do not match!")
                print(
                    "loaded hyperparams: ",
                    json.dumps(loaded_hyperparams, sort_keys=True),
                )
                print("current hyperparams: ", json.dumps(hyperparams, sort_keys=True))
                print("ABORTING!")
                exit(1)
    else:
        with open(metadatafile, "w") as outfile:
            json.dump(hyperparams, outfile, sort_keys=True)

    all_agents = [
        int(filename[len(checkpointfilename_prefix) :].split(".")[0])
        for filename in os.listdir(checkpoint_path)
        if os.path.isfile(os.path.join(checkpoint_path, filename))
        and filename.startswith(checkpointfilename_prefix)
    ]

    latest_trained_agent = max(all_agents) if all_agents else 0
    print("latest_trained_agent: ", latest_trained_agent)

    env_fn = hyperparams_to_envfn(hyperparams)
    env = SubprocVecEnv([env_fn] * hyperparams["nenvs"])
    env = VecNormalize(env, training=True, norm_obs=True, norm_reward=True)

    # https://stable-baselines.readthedocs.io/en/master/guide/custom_policy.html
    policy_kwargs = dict(net_arch=hyperparams["net_arch"])

    # Parameters:
    # policy                        – (ActorCriticPolicy or str) The policy model to use (MlpPolicy, CnnPolicy, CnnLstmPolicy, …)
    # env                           – (Gym environment or str) The environment to learn from (if registered in Gym, can be str)
    # gamma = 0.99                  – (float) Discount factor
    # n_steps = 128                 – (int) The number of steps to run for each environment per update (i.e. batch size is n_steps * n_env where n_env is number of environment copies running in parallel)
    # ent_coef = 0.01               – (float) Entropy coefficient for the loss calculation
    # learning_rate = 0.00025       – (float or callable) The learning rate, it can be a function
    # vf_coef = 0.5                 – (float) Value function coefficient for the loss calculation
    # max_grad_norm = 0.5           – (float) The maximum value for the gradient clipping
    # lam = 0.95                    – (float) Factor for trade-off of bias vs variance for Generalized Advantage Estimator
    # nminibatches = 4              – (int) Number of training minibatches per update. For recurrent policies, the number of environments run in parallel should be a multiple of nminibatches.
    # noptepochs = 4                – (int) Number of epoch when optimizing the surrogate
    # cliprange = 0.2               – (float or callable) Clipping parameter, it can be a function
    # cliprange_vf = None           – (float or callable) Clipping parameter for the value function, it can be a function. This is a parameter specific to the OpenAI implementation. If None is passed (default), then cliprange (that is used for the policy) will be used. IMPORTANT: this clipping depends on the reward scaling. To deactivate value function clipping (and recover the original PPO implementation), you have to pass a negative value (e.g. -1).
    # verbose = 0                   – (int) the verbosity level: 0 none, 1 training information, 2 tensorflow debug
    # tensorboard_log = None        – (str) the log location for tensorboard (if None, no logging)
    # _init_setup_model = True      – (bool) Whether or not to build the network at the creation of the instance
    # policy_kwargs = None          – (dict) additional arguments to be passed to the policy on creation
    # full_tensorboard_log = False  – (bool) enable additional logging when using tensorboard WARNING: this logging can take a lot of space quickly
    # seed = None                   – (int) Seed for the pseudo-random generators (python, numpy, tensorflow). If None (default), use random seed. Note that if you want completely deterministic results, you must set n_cpu_tf_sess to 1.
    # n_cpu_tf_sess = None          – (int) The number of threads for TensorFlow operations If None, the number of cpu of the current machine will be used.

    # https://docs.google.com/spreadsheets/d/1fNVfqgAifDWnTq-4izPPW_CVAUu9FXl3dWkqWIXz04o/edit#gid=0
    # noptepochs - could be something like the replay memory - should only be larger (10-20) if batchsize is very large
    # first n_steps * n_env is collected, then sgd updates are done in nminibatches size batches for n epochs
    # for i in noptepochs
    #   for minibatch in batch
    #       sgd(minibatch)

    ppo_params = {
        "n_steps": hyperparams["n_steps"],
        "nminibatches": hyperparams["nminibatches"],
        "noptepochs": hyperparams["noptepochs"],
        "ent_coef": hyperparams["beta"],
    }
    timesteps = (
        hyperparams["numepisodes"] * 8
    )  # TODO FIXME actually 8 should be replaced by nevns

    if latest_trained_agent > 0:
        checkpointfile = os.path.join(
            checkpoint_path, checkpointfilename.format(latest_trained_agent)
        )
        # TODO loading with all params might not work (set parameters method!)
        model = PPO2.load(
            checkpointfile,
            env=env,
            **ppo_params,
            verbose=1,
            tensorboard_log=tensorboard_log_dir,  # TODO submit Black bug not adding comma here but complaining on check
        )
    else:
        model = PPO2(
            MlpPolicy,
            env,
            **ppo_params,
            policy_kwargs=policy_kwargs,
            verbose=1,
            tensorboard_log=tensorboard_log_dir,
        )

    for i in range(latest_trained_agent + 1, num_runs + 1):
        # TODO last agent needs to be updated every run!

        # Pass reset_num_timesteps=False to continue the training curve in tensorboard
        # By default, it will create a new curve
        print()
        runstr = "RUN {}".format(i)
        _l = len(runstr)
        lpad = _l // 2
        rpad = _l - lpad
        print("##{}{}{}##".format(" " * lpad, runstr, " " * rpad))
        print()

        model.learn(
            total_timesteps=timesteps,
            log_interval=1,
            reset_num_timesteps=False,
            tb_log_name="run#{}".format(i),
        )
        model.save(os.path.join(checkpoint_path, checkpointfilename.format(i)))
