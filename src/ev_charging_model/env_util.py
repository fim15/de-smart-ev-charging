import gym


def hyperparams_to_envfn(hyperparams):
    env_fn = lambda: gym.make("ev_charging_gym:EVCharging-v0")
    if "gym_kwargs" in hyperparams:

        def make_env(gym_kwargs):
            def _init():
                env_id = "CustomEVCharging-v0"
                gym.envs.register(
                    id=env_id,
                    entry_point="ev_charging_gym:EVChargingEnv",
                    kwargs=gym_kwargs,
                )
                env = gym.make(env_id)
                return env

            return _init

        env_fn = make_env(hyperparams["gym_kwargs"])

        print("registering new environment")
    return env_fn
