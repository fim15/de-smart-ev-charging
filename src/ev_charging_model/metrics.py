from ev_charging_engine.simplestate import SimpleState

N = SimpleState.steps_per_week()


def sum_weekly(results):
    lf_series = results.pop("loadfactor")
    results["loadfactor_mean"] = [0 for _ in range(len(lf_series))]
    results["loadfactor_max"] = [0 for _ in range(len(lf_series))]
    for i in range(0, len(lf_series), N):
        results["loadfactor_max"][i + N - 1] = max(lf_series[i : i + N])
        results["loadfactor_mean"][i + N - 1] = sum(lf_series[i : i + N]) / N

    for f in results:
        series = results[f]
        results[f] = [sum(series[i : i + N]) for i in range(0, len(series), N)]
    return results


def extra_metrics(results):
    results["cost/kw"] = [
        c / kw for c, kw in zip(results["cost"], results["electricity_used"])
    ]
    results["no_peak_cost"] = [
        c - p for c, p in zip(results["cost"], results["peak_cost"])
    ]
    results["profit"] = [
        30 * kw - c for c, kw in zip(results["cost"], results["electricity_used"])
    ]
    results["%guarantee_cost"] = [
        g / c for g, c in zip(results["guarantee_cost"], results["cost"])
    ]
    results["%guarantee_electricity_used"] = [
        g / e
        for g, e in zip(
            results["guarantee_electricity_used"], results["electricity_used"]
        )
    ]
    results["Profit"] = results.pop("profit")
    results["# of Charged EVs"] = results.pop("donecount")
    results["Station Load Mean"] = results.pop("loadfactor_mean")
    results["Station Load Max"] = results.pop("loadfactor_max")
    results["Demand Charge"] = results.pop("peak_cost")
    results["frac kWh Charged to Fulfill Guarantee"] = results.pop(
        "%guarantee_electricity_used"
    )
    results["Unused PV"] = results.pop("unused_pv")
    del results["guarantee_electricity_used"]
    del results["%guarantee_cost"]
    del results["guarantee_cost"]
    del results["no_peak_cost"]
    del results["cost/kw"]
    del results["cost"]
    del results["electricity_used"]
    # del results["unused_pv"]
    return results


def add_guarantee_peak(results):
    PEAK_COST = 550 / 4
    kw_used = results["guarantee_kw_bought"]
    results["guarantee_peak"] = [0.0 for i in range(len(kw_used))]
    for i in range(0, len(kw_used), N):
        results["guarantee_peak"][i + N - 1] = max(kw_used[i : i + N]) * PEAK_COST
    del results["guarantee_kw_bought"]
    return results


def preclean(results):
    del results["revenue"]
    return results


def to_metrics(results):
    results = preclean(results)
    results = add_guarantee_peak(results)
    results = sum_weekly(results)
    results = extra_metrics(results)
    return results
