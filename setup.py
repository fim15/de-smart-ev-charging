import os

from setuptools import find_packages, setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="evcharging",
    version="0.0.1",
    author="kechtel, clotodex",
    author_email="<>, <clotodex@online.de>",
    description=("Reinforcement Learning for large scale ev charging"),
    license="<none>",
    packages=find_packages("src"),
    package_dir={"": "src"},
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Utilities",
        # "License :: OSI Approved :: MIT License",
    ],
    install_requires=[
        requirement.strip() for requirement in read("requirements.txt").split('\n')
        if not (requirement.strip()=='' or requirement.startswith('#'))
    ],
)
