import os

import pandas as pd
import tensorflow as tf

df = pd.DataFrame(columns=["step", "metric", "value"])

summary_dir = "models/test/test_v15min_PPO2_ep102400_envs32_nst1024_128_128-128_128-128_b0.01_fevs0.002/"
for filename in os.listdir(summary_dir):
    path = os.path.join(summary_dir, filename)
    try:
        for e in tf.train.summary_iterator(path):
            for v in e.summary.value:
                # print(f"tag: {v.tag}: {v.simple_value}, step: {e.step}")
                df = df.append(
                    {"step": e.step, "metric": v.tag, "value": v.simple_value},
                    ignore_index=True,
                )
    except tf.errors.DataLossError as e:
        print("error")

df.info()
print(df.head())

print()
max_step = df["step"].max()
df = df[df["step"] == max_step]
print(df)
