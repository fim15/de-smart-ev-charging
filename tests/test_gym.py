import gym
from stable_baselines.common.env_checker import check_env
from stable_baselines.common.vec_env import DummyVecEnv, VecNormalize


def test_check_gym():
    env = gym.make("ev_charging_gym:EVCharging-v0")
    # It will check your custom environment and output additional warnings if needed
    check_env(env)


def test_vec_env():
    nenvs = 4
    # TODO include test with SubprocVecEnv (runs on gitlab?)
    env = DummyVecEnv([lambda: gym.make("ev_charging_gym:EVCharging-v0")] * nenvs)
    env = VecNormalize(env, training=True, norm_obs=True, norm_reward=True)

    env.reset()
    for i in range(10):
        env.step([env.action_space.sample()] * nenvs)
    env.close()


def test_gym():
    env = gym.make("ev_charging_gym:EVCharging-v0")
    obs = env.reset()  # ,info

    print("observation space: ", env.observation_space)
    print("action space: ", env.action_space)

    assert env.observation_space is not None
    assert env.action_space is not None

    # TODO adjust actions to charging rule

    total_reward = 0.0
    done = False
    # while not done:   # TODO use this if done is set
    for i in range(24 * 7):
        env.render()
        obs, reward, done, info = env.step(env.action_space.sample())
        assert done or not any(x is None for x in obs)
        # test for out of bounds fail
        total_reward += reward
        print("reward: ", reward)
    env.close()
    print("total reward: ", total_reward)
