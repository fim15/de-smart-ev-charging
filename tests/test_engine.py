from ev_charging_engine.charging_rules import ConstantChargingRule
from ev_charging_engine.evc_station import EVC_Station


def test_week():
    rule = ConstantChargingRule(10)
    station = EVC_Station(rule, 100)
    station.reset()
    for i in range(24 * 7):
        station.timestep()
