.NOTPARALLEL:

.PHONY : install pytest external all train clean workflow

.DEFAULT_GOAL := all

install:
	python setup.py install

pytest:
	pytest tests

all: | install pytest

train:
	python src/ev_charging_model/train.py

test:
	python src/ev_charging_model/test.py -c "checkpoints/" -b "ppo2_evcharging_tensorboard/test/" -t -p "ppo2_evcharging_" -s ".npy"

clean:
	pip uninstall -y evcharging

workflow: | clean all train
